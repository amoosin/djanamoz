# https://pypi.org/

# Django>=2.2.11,<2.3.0
# psycopg2==2.8.5
# Pillow>=7.0.0,<7.1.0
# flake8>=3.7.9,<3.8.0
# django-allauth>=0.42.0,<0.43.0
