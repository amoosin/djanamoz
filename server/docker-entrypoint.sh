#!/bin/sh

# درنگیدن تا آماده‌شدن سرور پایگاه داده
if [ "$DB_ENGINE" = "django.db.backends.postgresql" ] && [ $DB_HOST ] && [ $DB_PORT ]; then
  while ! nc -z $DB_HOST $DB_PORT; do
    sleep 0.1
  done
fi

# آماده‌سازی برنامه
if [ -f "manage.py" ]; then
  python manage.py flush --no-input
  python manage.py migrate
  python manage.py collectstatic --no-input --clear
fi

exec "$@"